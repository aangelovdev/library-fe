import { Routes, Route } from "react-router-dom";
import Login from './Login';
import Register from './Register';
import Library from './Library';
import Book from './Book';
import Settings from './Settings';
import ErrorPage from './ErrorPage';
import useToken from './useToken';


function App() {

  const { token } = useToken();

  return (
    <Routes>
      <Route path="/" element={<Login />} />
      <Route path="/login" element={<Login />} />

      {token && <Route>
        <Route path="/register" element={<Register />} />
        <Route path="/library" element={<Library />} />
        <Route path="/book/:id" element={<Book />} />
        <Route path="/settings" element={<Settings />} />  
      </Route>}

      <Route path="*" element={<ErrorPage />} />
    </Routes>
  );
}

export default App;
