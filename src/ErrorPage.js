import React from 'react'

function ErrorPage() {
  return (
    <div>
        Error 404. The page you're looking for can't be found.
    </div>
  )
}

export default ErrorPage