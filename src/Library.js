import { Component } from 'react';
import Navbar from "./Navbar";
import useToken from './useToken';
import React, { useState, useEffect,} from 'react';
import { useNavigate } from 'react-router-dom';
import moment from 'moment';

async function getAllBooks(token) {
  return fetch('https://books-library-dev.herokuapp.com/api/book', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token,
    },
    // body: { "pattern": "c" } za search
  }).then(data => data.json())
}

function Library() {

  const { token } = useToken();
  const [allBooks, setAllBooks] = useState([]);

  useEffect(() => {
    getAllBooks(token).then(response => setAllBooks(response));
  }, []);

  let navigate = useNavigate();

  return (
    <>
      <Navbar />

      <div className="books-container">
        <div className="search-container">
          <h3>ALL BOOKS</h3>
          <input id="search-button" type="text" placeholder="Search.." name="search" />
        </div>

        <div className="books-list">
          {allBooks.map(book =>
            <div className="book">
              <div className="book-container">
                <div className="book-image">
                  <img src={book.image} />
                </div>
                <div className="book-content">
                  <h4 id="book-name">{book.name}</h4>
                  <p>{book.author}</p>
                  <p>Genre: <strong>{book.genre.name}</strong></p>
                  <p>Created on: <strong>{moment(book.createOn).format("DD.MM.YYYY")}</strong> </p>
                  <p>Updated on: <strong>{moment(book.lastUpdateOn).format("DD.MM.YYYY")}</strong> </p>
                </div>

                <div className="book-button">
                  <button id="button-book" className="btn-primary" onClick={() => {navigate(`/book/${book._id}`)}} type="button"><i className="fa fa-play" aria-hidden="true"></i></button>
                </div>
              </div>
            </div>
          )}

        </div>
      </div>
    </>
  );


}


export default Library;