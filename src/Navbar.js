import logo from './assets/img/logo.png';
import { useNavigate } from 'react-router-dom';
import useToken from './useToken';

function Navbar({ showBackButton }) {
  
  const navigate = useNavigate();
  const { setToken } = useToken();

  function logout() {
    setToken('');
    navigate('/login');
  }

  return (
    <nav>
      <div className="navbar">

        {showBackButton ? <button className="book-navbar-btn">
          <span class="button-icon" onClick={() => { navigate(`/library/`) }}><i class="fa fa-caret-left" aria-hidden="true"></i> Library</span>
        </button> : <img id="lib-logo" src={logo} />}

        <ul className="menu">
          <li className="active"><a href="/library">LIBRARY</a></li>
          <li className="active"><a href="/settings">SETTINGS</a></li>
        </ul>


        <i onClick={() => logout()} className="fa fa-sign-out" aria-hidden="true"></i>
      </div>
    </nav>
  );

}

export default Navbar;
