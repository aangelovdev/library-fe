import React from 'react'
import Navbar from "./Navbar";

function Settings() {
    return (
        <><Navbar />
            <div className="settings-container">
                <div className="general-settings">
                    <h3><strong>GENERAL SETTINGS</strong></h3> <br/>
                    <p><strong>NOTIFICATIONS AND EMAILS</strong></p>
                    <p><strong>USER MANAGEMENT</strong></p>
                    <p><strong>PHYSICAL LIBRARIES</strong></p>
                    <p><strong>READING EVENTS</strong></p>
                    <p><strong>INVOICING</strong></p>
                    <p><strong>BOOK STATISTICS</strong></p>
                    <p><strong>READERS STATISTICS</strong></p>
                    <p><strong>EVENTS STATISTICS</strong></p>

                </div>

                <div className="book-settings">
                    <h3><strong>BOOK SETTINGS</strong></h3> <br/>
                    <p><strong>MANAGE GENRES</strong></p>
                    <p><strong>BOOK VISIBILITY</strong></p>
                    <p><strong>AUTHORS DATABASE</strong></p>
                    <p><strong>BOOK COVERS</strong></p>
                    <p><strong>BOOK COVERS</strong></p>
                </div>
            </div></>
    )
}

export default Settings