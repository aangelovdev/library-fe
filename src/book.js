import { Component } from 'react';
import Navbar from "./Navbar";
import useToken from './useToken';
import React, { useState, useEffect } from 'react';
import moment from 'moment';
import { useParams } from "react-router-dom";


async function getSingleBook(token, id) {
  return fetch('https://books-library-dev.herokuapp.com/api/book/' + id, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token,
    },
  }).then(data => data.json())
}

function BookDetails() {

  const { token } = useToken();
  const [book, setBook] = useState();
  const { id } = useParams();

  useEffect(() => {
    getSingleBook(token, id).then(response => setBook(response));
  }, []);

  return (
    <>
      <Navbar showBackButton = {true}/>
      {book &&
        <div className="book-info-container">
          <div className="info-left-container">
            <form className="info-content">
              <img src={book.image} />
            </form>
          </div>

          <div className="info-right-container">
            <div className="book-info-title">
              <h2 id="book-info-h">{book.name}</h2>
            </div>
            <div className="book-info-content">
              <h2>{book.author}</h2>
              <p>Genre: <strong>{book.genre.name}</strong></p>
              <p>Created on: <strong>{moment(book.createOn).format("DD.MM.YYYY")}</strong> </p>
              <p>Updated on: <strong>{moment(book.lastUpdateOn).format("DD.MM.YYYY")}</strong> </p>
            </div>
            <div className="book-info-description">
              <p><strong>Short description</strong></p>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
              </p>
            </div>

          </div>

        </div>
      }

    </>
  );


}


export default BookDetails;